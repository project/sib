<?php

if ($sidebar_left && $sidebar_right)
	$layoutstyle = "bothsidebars";
else if ($sidebar_left)
	$layoutstyle = "leftsidebar";
else if ($sidebar_right)
	$layoutstyle = "rightsidebar";
else
	$layoutstyle = "nosidebars";


$topregionblocks = 0;
if($user1) $topregionblocks += 1;
if($user2) $topregionblocks += 1;
if($user3) $topregionblocks += 1;
switch ($topregionblocks) {
	case 1:
		$topregionwidth = "width100";
		break;
	case 2:
		$topregionwidth = "width50";
		break;
	case 3:
		$topregionwidth = "width33";
		break;
	default:
		$topregionwidth = "";
}

$topregion12seperator = "";
$topregion23seperator = "";
if ($user1 && $user2 || $user3) {
	$topregion12seperator = "topregionseperator";
}
if ($user2 && $user3) {
	$topregion23seperator = "topregionseperator";
}

$bottomregionblocks = 0;
if($user4) $bottomregionblocks += 1;
if($user5) $bottomregionblocks += 1;
if($user6) $bottomregionblocks += 1;
switch ($bottomregionblocks) {
	case 1:
		$bottomregionwidth = "width100";
		break;
	case 2:
		$bottomregionwidth = "width50";
		break;
	case 3:
		$bottomregionwidth = "width33";
		break;
	default:
		$bottomregionwidth = "";
}

$bottomregion12seperator = "";
$bottomregion23seperator = "";
if ($user4 && $user5 || $user6) {
	$bottomregion12seperator = "bottomregionseperator";
}
if ($user5 && $user6) {
	$bottomregion23seperator = "bottomregionseperator";
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language ?>" lang="<?php print $language ?>">
<head>
  <title><?php print $head_title; ?></title>
  <?php drupal_add_css(drupal_get_path('theme', 'siberia') . '/css/reset-fonts-grids.css', 'theme'); ?>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>

   <?php if (theme_get_setting('siberia_width')) { ?>
	<style type="text/css">
      	#page {
			width : <?php print theme_get_setting('siberia_fixedwidth') ?>px;
		}
      </style>
   <?php } else { ?>
	<style type="text/css">
      	#page {
      width: 95%;
		}
      </style>
   <?php }  ?>

<?php if (theme_get_setting('siberia_iepngfix')) { ?>
<!--[if lte IE 7]>
<script type="text/javascript">
    $(document).ready(function(){
        $(document).pngFix();
    });
</script>
<![endif]-->
<?php } ?>


</head>

<body class="<?php print $layoutstyle; ?>">
  <div id="page">
    <div id="masthead">
    <div id="header" class="clear-block">
        <div class="header-left">
        <div class="header-right">


      <div id="logo-title">
        <?php if ($logo): ?>
          <a href="<?php print $base_path; ?>" title="<?php print t('Home'); ?>">
            <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" id="logo" />
          </a>
        <?php endif; ?>
      </div> <!-- /logo-title -->

        <div id="name-and-slogan">
        <?php if ($site_name): ?>
          <h2 id='site-name'>
            <a href="<?php print $base_path ?>" title="<?php print t('Home'); ?>">
              <?php print $site_name; ?>
            </a>
          </h2>
        <?php endif; ?>
        <?php if ($site_slogan): ?>
          <div id='site-slogan'>
            <?php print $site_slogan; ?>
          </div>
        <?php endif; ?>
        </div> <!-- /name-and-slogan -->

      <?php if ($header): ?>
            <?php print $header; ?>
      <?php endif; ?>

    </div> <!-- /header-left -->
    </div> <!-- /header-right -->
    </div> <!-- /header -->
  </div>

      <div id="navigation" class="menu <?php if ($primary_links) { print "withprimary"; } if ($secondary_links) { print " withsecondary"; } ?> ">
          <?php if ($primary_links): ?>
            <div id="primary" class="clear-block">
              <?php print theme('menu_links', $primary_links); ?><?php print $search_box; ?>
            </div>
          <?php endif; ?>
      </div> <!-- /navigation -->


      <?php if($topregionblocks) { ?>
		<div id="topregion" class="clear-block">
			<table class="regions" cellspacing="0" cellpadding="0">
				<tr valign="top">
            <?php if ($user1): ?>
               <td class="region <?php echo $topregionwidth; ?>">
                  <?php print $user1; ?>
               </td>
            <?php endif; ?>

            <?php if ($user2): ?>
               <td class="region <?php echo $topregionwidth; ?>">
                  <?php print $user2; ?>
               </td>
            <?php endif; ?>

            <?php if ($user3): ?>
               <td class="region <?php echo $topregionwidth; ?>">
                  <?php print $user3; ?>
               </td>
            <?php endif; ?>
				</tr>
			</table>
		</div>
      <?php } ?>

      <?php if ($secondary_links): ?>
         <div id="secondary" class="clear-block">
            <?php print theme('menu_links', $secondary_links); ?>
         </div>
      <?php endif; ?>


    <div id="middlecontainer" class="clear-block">
    <div id="centerpart">

      <?php if ($sidebar_left): ?>
        <div id="leftsidebar" class="sidebar">
          <?php print $sidebar_left; ?>
        </div> <!-- /sidebar-left -->
      <?php endif; ?>

      <div id="maincontent" >
	    <div id="squeeze">
	      <?php if ($mission != ""): ?>
		  <div class="mission"><?php print $mission ?></div>
          <?php endif; ?>
          <?php if ($content_top):?><div id="content-top"><?php print $content_top; ?></div><?php endif; ?>
          <?php if ($title): ?><h1 class="title"><?php print $title; ?></h1><?php endif; ?>
          <?php if ($tabs): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
          <?php print $content; ?>
          <?php if ($content_bottom): ?><div id="content-bottom"><?php print $content_bottom; ?></div><?php endif; ?>
        </div>
	  </div>

      <?php if ($sidebar_right): ?>
        <div id="rightsidebar" class="sidebar">
          <?php print $sidebar_right; ?>
        </div> <!-- /sidebar-right -->
      <?php endif; ?>
    </div>

    </div> <!-- /middlecontainer -->




      <?php if($bottomregionblocks) { ?>
		<div id="bottomregion" class="clear-block">
			<table class="regions" cellspacing="0" cellpadding="0">
				<tr valign="top">
            <?php if ($user4): ?>
               <td class="region <?php echo $bottomregionwidth; ?>">
                  <?php print $user4; ?>
               </td>
            <?php endif; ?>

            <?php if ($user5): ?>
               <td class="region <?php echo $bottomregionwidth; ?>">
                  <?php print $user5; ?>
               </td>
            <?php endif; ?>

            <?php if ($user6): ?>
               <td class="region <?php echo $bottomregionwidth; ?>">
                  <?php print $user6; ?>
               </td>
            <?php endif; ?>
				</tr>
			</table>
		</div>
      <?php } ?>


   <div id="footer" class="clear-block">
    <?php print $footer_message; ?>
	<br /><a href="http://www.homed.ru">template Siberia</a>
   </div>

    <div id="footer-wrapper" class="clear-block">
        <div class="footer-right">
            <div class="footer-left">

            </div> <!-- /footer-left -->
        </div> <!-- /footer-right -->
    </div> <!-- /footer-wrapper -->


  </div> <!-- /page -->

</body>

</html>
