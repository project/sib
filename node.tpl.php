<div class="<?php print $node_classes ?>" id="node-<?php print $node->nid; ?>">

<?php if ($page == 0): ?>

  <?php
  if($type!='storymin') {
  ?>
    <h1 class="title">
      <a href="<?php print $node_url ?>"><?php print $title; ?></a>
    </h1>
  <?php
  } else {
  ?>
    <h1 class="title">
      <?php print $title; ?>
    </h1>
  <?php
  }
  ?>

<?php endif; ?>

  <?php if ($picture) print $picture; ?>




<?php if ($type!='storymin'): ?>
  <?php if ($submitted): ?>
    <span class="submitted"><?php print format_date($node->created, 'custom', "F jS, Y") . theme('username', $node); ?></span>
  <?php endif; ?>

  <?php if (count($taxonomy)): ?>
    <div class="taxonomy"><?php print t(' in ') . $terms ?></div>
  <?php endif; ?>
  <?php endif; ?>

  <div class="content">
    <?php print $content; ?>
  </div>

<?php if ($type!='storymin'): ?>
  <?php if ($links): ?>
    <div class="links">
      <?php print $links; ?>
    </div>
  <?php endif; ?>
  <?php endif; ?>

</div>
