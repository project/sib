<?php

if (theme_get_setting('siberia_iepngfix')) {
   drupal_add_js(drupal_get_path('theme', 'siberia') . '/js/jquery.pngFix.js', 'theme');
}
$siberia_style = "gray";
drupal_add_css(drupal_get_path('theme', 'siberia') . '/css/' . $siberia_style . '.css', 'theme');


function siberia_regions() {
  return array(
       'left' => t('left sidebar'),
       'right' => t('right sidebar'),
       'content_top' => t('content top'),
       'content_bottom' => t('content bottom'),
       'header' => t('header'),
       'footer' => t('footer'),
       'navmenu' => t('navmenu'),
       'user1' => t('user1'),
       'user2' => t('user2'),
       'user3' => t('user3'),
       'user4' => t('user4'),
       'user5' => t('user5'),
       'user6' => t('user6')
  );
}


?>
